<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;
class PostController extends Controller
{
    public function create(){
    	return view('posts.create');
    }

    public function store (Request $request){
    	// dd($request->all());
    	$this->validate($request, [
		'title' => 'required|unique:posts|max:255',
		'body' => 'required',
		]);

    	$query = DB::table('posts')->insert([
    		"title" => $request["title"],
    		"body" => $request["body"]
    	]);
    	return redirect('/posts')->with('success','post berhasil disimpan');
    }
    public function index(){
    	$posts = DB::table('posts')->get();
    	return view('posts.index', compact('posts'));
    }
    public function show($id){
    	$post = DB::table('posts')->where('id', $id)->first();
    	return view('posts.show', compact('post'));
    }

    public function edit($id){
    	$post = DB::table('posts')->where('id', $id)->first();
    	return view('posts.edit', compact('post'));
    }

     public function update($id, Request $request){
     	$this->validate($request, [
		'title' => 'required|unique:posts|max:255',
		'body' => 'required',
		]);
    	$query = DB::table('posts')
            ->where('id', $id)
            ->update([
            	'title' => $request['title'],
            	'body' => $request['body']
            ]);
    	return redirect('/posts')->with('success', 'berhasil update posts');
    }
    public function destroy($id) {
    	$query = DB::table('posts')->where('id', $id)->delete();
    	return redirect('/posts')->with('success', 'berhasil delete posts');
	}
}
